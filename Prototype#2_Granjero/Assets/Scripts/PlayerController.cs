﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    public float xRange = 5; // limita los valores hacia la izquierda (valores negativos) y hacia la derecha (valores positivos) 
    public float zRange = 0.5f; // limita los valores hacia detras (valores negativos) y hacia adelante(valores positivos)
    public float horizontalInput;
    float verticalInput;
    public float speed = 10f;
    Vector3 currentOffset; // para limitar la posicion del player en cualquier lugar de la escena
    public GameObject projectilePrefab; //convertir a prefab la comida, y sirve para lanzarlo
    public GameObject otherProjectilePrefab;
    float count = 0;
    
    private void Start()
    {
        currentOffset = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && count % 2 != 0)
        {
            Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
            count++;
        }
        else if (Input.GetKeyDown(KeyCode.Space) && count % 2 == 0)
        {
            Instantiate(otherProjectilePrefab, transform.position, otherProjectilePrefab.transform.rotation);
            count++;
        }
            
        
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.right * speed * Time.deltaTime * horizontalInput);
        transform.Translate(Vector3.forward * speed * Time.deltaTime * verticalInput);

        if (transform.position.x < currentOffset.x - xRange) // entra en el if cuando me salgo por la izquierda
        {
            transform.position = new Vector3(currentOffset.x - xRange, transform.position.y, transform.position.z);
        }
        if (transform.position.x > currentOffset.x + xRange) // entra en el if cuando me salgo por la derecha
        {
            transform.position = new Vector3(currentOffset.x + xRange, transform.position.y, transform.position.z);
        }

        if (transform.position.z > currentOffset.x + zRange) // entro en el if cuando me salgo por delante
        {
            transform.position = new Vector3(transform.position.y, transform.position.x + currentOffset.x + zRange);
        }
        if (transform.position.z < currentOffset.x - zRange) // entro en el if cuando me salgo por detras
        {
            transform.position = new Vector3(transform.position.y, transform.position.x - currentOffset.x - zRange);
        }

        

    }
}
