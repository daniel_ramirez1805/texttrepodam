﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio4 : MonoBehaviour
{
    // 4. Escriba un programa que dada una edad indique si es mayor de edad o no.
    public int age = 11;
    // Start is called before the first frame update
    void Start()
    {
        if(age > 18)
        {
            Debug.Log(age + " is older ");
        }
        else
        {
            Debug.Log(age + " is not older ");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
