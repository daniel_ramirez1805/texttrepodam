﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio5 : MonoBehaviour
{
    // 5. Escriba un programa que dado un entero devuelva -1 por Log si este es negativo, 0 si el entero vale 0 o 1 si es positivo.
    public int number = -1;
    // Start is called before the first frame update
    void Start()
    {
        if(number < 0 )
        {
            Debug.Log("-1");
        }
        else if (number >= 0)
        {
            Debug.Log("1");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
