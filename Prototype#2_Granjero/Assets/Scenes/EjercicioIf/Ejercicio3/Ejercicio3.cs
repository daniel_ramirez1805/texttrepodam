﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio3 : MonoBehaviour
{
    // 3. Escriba un programa que indique si un año es bisiesto o no.
    public int year = 2016;
    // Start is called before the first frame update
    void Start()
    {
        if(year % 4 == 0 || year % 100 == 0 && year % 400 != 0 )
        {
            Debug.Log("The year is leap " + year);
        } else
        {
            Debug.Log("The year isn´t leap " + year);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
