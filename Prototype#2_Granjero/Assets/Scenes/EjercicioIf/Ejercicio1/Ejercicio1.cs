﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio1 : MonoBehaviour
{
    // 1. Escriba un programa que acepte dos enteros por inspector y compruebe si son iguales o no.
    public int number = 5;
    public int otherNumber = 5;
    // Start is called before the first frame update
    void Start()
    {
        if (number > otherNumber)
        {
            Debug.Log(number + " is higher");
        }
        else if (number < otherNumber)
        {
            Debug.Log(number + " is smaller");
        }
        else {
            Debug.Log("5 and 5 are equal");
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
