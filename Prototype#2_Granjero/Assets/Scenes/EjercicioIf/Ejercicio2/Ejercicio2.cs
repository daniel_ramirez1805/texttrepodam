﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ejercicio2 : MonoBehaviour
{
    //2. Escriba un programa que diga si un número es positivo o negativo.
    // Start is called before the first frame update
    public int number = 14;
    void Start()
    {
        if (number > 0)
        {
            Debug.Log(number + " is positive");
        }
        else if (number < 0)
        {
            Debug.Log(number + " is negative");
        }
        else {
            Debug.Log(number + " is neutral");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
